---
title: "Examen final de statistiques et de cartographie"
format: pdf

---
```{r, echo=F}
knitr::opts_chunk$set(echo = FALSE,  eval=TRUE, warning = FALSE, message = FALSE)

```

## (1) PREPARATION  DES DONNEES (1 pt)

-   **Consigne** : Après avoir chargé le tableau excel *tun_del_2004.xls*, sélectionnez votre gouvernorat et construisez un tableau permettant d'étudier l'équipement des ménages en ordinateur en 2004 et 2014.

-   **Résultat** :

```{r}
# Importe les données
library(readxl)
data<-read_xls(path = "data/don_del.xls",
              sheet = "data")

# Liste des gouvernorats
# names(table(data$gou_nom))

# Choisit un gouvernorat !
choix_gou <- "Sfax"

# Sélectionne les lignes
don <- data[data$gou_nom==choix_gou,]

# simplifie le code
don$code<-substr(don$del_code,7,8)

# Calcule le taux d'équipement
don$equip_2004 <- 100*don$ordin_2004/don$menag_2004
don$equip_2014 <- 100*don$ordin_2014/don$menag_2014

# Sélectionne les colonnes
don <- don[,c("code","del_nom_fr","del_nom_ar","gou_cap","menag_2004","menag_2014", "ordin_2004", "ordin_2014")]

# renomme
names(don)<-c("code","nomfr","nomar","cap", "men04","men14","equ04","equ14")

# Calcul les taux par ménage
don$pct04<-100*don$equ04/don$men04
don$pct14<-100*don$equ14/don$men14

# Affiche le tableau
library(knitr)
kable(don, 
      main = "Tableau 1 : taux d'équipement des ménages",
      digits=c(0,0,0,0,0,0,0,0,1,1))
```



{{< pagebreak >}}

## (2) STATISTIQUE UNIVARIEE (4 pts)


### Paramètres principaux :

-   **Consigne** : Calculez la moyenne, l'écart-type et le coefficient de variation du taux d'équipement des ménages en 2004 et 2014 et présentez-les dans un tableau. 

```{r}
# sélectionne les variables
sel <- don[,c("pct04","pct14")]

# Tableau standard
moy<-apply(sel,2,mean)
ect<-apply(sel,2,sd)
cv<-100*ect/moy
tab<-rbind(moy,ect,cv)
row.names(tab) <-c("Moyenne","Ecart-type", "C.V. (%)")

kable(tab, caption="Paramètres principaux", digits =1,
      col.names = c("Situation en 2004","Situation en 2014"),
      )
```

### Histogrammes

-   **Consigne** :Etablissez deux histogrammes permettant de comparer la distribution du taux d'équipement des ménages en 2004 et 2014. 

```{r, fig.width=6, fig.height=3}
par(mfrow=c(1,2))

mintot <-min(c(sel$pct04, sel$pct14))
maxtot <-max(c(sel$pct04, sel$pct14))

# Histogramme
hist(sel$pct04,
     breaks=quantile(sel$pct04),
     xlim=c(mintot,maxtot),
     col="gray80",
     main= "Situation en 2004",
     xlab = "Taux d'équipement (%)",
     ylab = "Fréquence moyenne")
rug(sel$pct04, col="black", lwd=2)
lines(density(sel$pct04), lty=3,lwd=2)

hist(sel$pct14,
    breaks=quantile(sel$pct14),
     xlim=c(mintot,maxtot),
     col="gray80",
     main= "Situation en 2014",
     xlab = "Taux d'équipement (%)",
     ylab = "Fréquence moyenne")
rug(sel$pct14, col="black", lwd=2)
lines(density(sel$pct14), lty=3,lwd=2)
```

### Commentaire

Que vous apprennent les paramètres statistiques et les histogrammes sur l'évolution du taux d'équipement des des ménages dans ce gouvernorat entre 2004 et 2014 ?




{{< pagebreak >}}

## (3) CARTOGRAPHIE (7 pts)


### Carte de repérage (1 pt)

-   **Consigne** : Après avoir chargé le shapefile *Tunisie2014_del.shp*, extraire les délégations correspondant à votre gouvernorat et afficher le fonds de carte avec le code des unités. Ajoutez ensuite un tableau avec le code et le nom des unités en français et en arabe.

```{r, fig.height=4}
# Chargement du package spatial features
library(sf)

# Importation du fonds de carte complet
map<-st_read("data/map_del.geojson", quiet=T)

# Selection d'un gouvernorat
map <- map[map$gou_nom == choix_gou,]

# simplifie le code
map$code <- substr(map$del_code,7,8)

# ne conserve que le code,la capitale et la géométrie
map <- map[,c("code","gou_cap","geometry")]


# Affichage du fonds de carte
par(mar=c(0,0,3,0))
plot(map$geometry, 
     col="gray90",
     main = "Code des unités spatiales de la zone d'étude")

# Ajout du code des unités spatiales
coo<-st_coordinates(st_centroid(map))
text(coo, map$code, cex=0.5,col="black",)

# Affichage des codes
x<-don$code
names(x)<-don$nomfr
x
```



{{< pagebreak >}}


### Cartes de stock (3 pts)

-   **Consigne** : Réalisez deux cartes de stock décrivant le nombre de ménages équipés en ordinateur en 2004 et 2014. Vous utiliserez la même échelle de taille pour rendre les deux cartes comparables.


```{r, fig.height=5, fig.width=8}
library(mapsf)
map<-map[,c("code","geometry")]
map_don <- merge(map, don, by="code")
maxequ<-max(don$equ04,don$equ14)

par(mfrow=c(1,2))
mf_map(map_don$geometry, col="white")
mf_map(map_don, type="prop", var="equ04",
       val_max = maxequ, inches=0.1, col="gray20", 
       leg_title = "Nb. de ménages équipés",)
mf_layout(title="2004",frame = T, credits = "Source : INS Tunisie")

mf_map(map_don$geometry, col="white")
mf_map(map_don, type="prop", var="equ14",
       val_max = maxequ, inches=0.1, col="gray20",
       leg_title = "Nb. de ménages équipés")
mf_layout(title="2014",frame = T, credits = "Source : INS Tunisie")


```

Que vous apprennent ces deux cartes sur l'évolution du nombre de ménages disposant de l'équipement dans ce gouvernorat entre 2004 et 2014 ?





{{< pagebreak >}}


### Cartes choroplèthes (3 pts)

-   **Consigne** : Réalisez deux cartes décrivant le pourcentage de ménages équipés en ordinateur en 2004 et 2014. Pour les rendre comparables vous utiliserez dans chaque carte une partition en quintiles (5 classes d'effectifs égaux)


```{r, fig.height=5, fig.width=8}
library(mapsf)
map_don <- merge(map, don, by="code")
maxequ<-max(don$equ04,don$equ14)

par(mfrow=c(1,2))
mf_map(map_don, type="choro", var="pct04",
       breaks = "quantile",nbreaks = 5, pal ="Grays",
       leg_title = "% ménages équipés",leg_val_rnd = 1)
mf_layout(title="2004",frame = T, credits = "Source : INS Tunisie")

mf_map(map_don, type="choro", var="pct14",
       breaks = "quantile",nbreaks = 5, pal ="Grays",
       leg_title = "% ménages équipés",leg_val_rnd = 1)
mf_layout(title="2014",frame = T, credits = "Source : INS Tunisie")

```

Que vous apprennent ces deux cartes sur l'évolution des inégalités d'équipement des ménages dans ce gouvernorat entre 2004 et 2014 ?




{{< pagebreak >}}

## (5) STATISTIQUES BIVARIEES (8 pts)

### Forme de la relation (2 pts)

- **Consigne** : Tracez un nuage de point montrant l'évolution de l'indicateur entre les deux dates puis calculez le coefficient de corrélation linéaire  et répondez à deux questions  :

```{r, fig.width=4, fig.width=4}
# prépration de l'analyse
code<-don$code
nomfr<-don$nomfr
nomar<-don$nomar
X<-don$pct04
Y<-don$pct14
tab<-data.frame(code,nomfr, nomar,X,Y)

# Diagramme
plot(tab$X,tab$Y, 
     pch=20,
     cex=0.8,
     col="red",
     main = "Evolution du taux d'équipement",
     xlab="tx. equipement. 2004",
     ylab ="tx. equipement 2014")
text(tab$X,tab$Y,tab$code, 
     pos=2,
     cex=0.5,
     col="blue")

abline(lm(Y~X),col="black",lwd=1)

r<-cor(X,Y)
paste("r(x,y) = ", round(r,3))
```


- La relation est-elle : positive ou négative ?






- La relation est-elle : forte ou faible ?






{{< pagebreak >}}



### Test de la relation (2 pts)

- **Consigne** : testez la relation à l'aide des coefficients de corrélation de Pearson et Spearman puis répondez aux deux questions. 

```{r}
cor.test(X,Y, method="pearson")
cor.test(X,Y, method="spearman")
```


- la relation est-elle : significative ou non significative ?





- Y-a-t-il des différences entre les valeurs des deux coefficients de corrélation. Si oui, pouquoi . 




{{< pagebreak >}}


### Droite de régression (2 pts)

- **Consigne** : calculez l'equation de la droite de régression et tracez- là sur le graphique établi précédemment. Puis répondez aux questions : 

```{r, fig.height=3, fig.width=3}
modreg <- lm(Y~X)
summary(modreg)

```


- L'équation de la droite de régression s'écrit Y = aX + b avec :  

 Equip2014 = .....  x  Equip2004 +  ......   (r2 = ........)
 
       
- Que signifie le coefficient  a ?




- Que signifie la constante b ?




- Que signifie le carré du coefficient de corrélation r2 ?





{{< pagebreak >}}

### Analyse des résidus (2 pts)

- **Consigne** :  Construisez un tableau des valeurs estimées et des résidus.  Commentez les résultats.

```{r}
tab$Y_est <- modreg$fitted.values
tab$Y_res <- modreg$residuals
tab<-tab[order(tab$Y_res),]
kable(tab, digits=1)
```



- Quelles délégations ont évolué plus favorablement que prévu ?





- Quelles délégations ont évolué moins favorablement que prévu ?





- Quelles délégations ont suivi la tendance générale





{{< pagebreak >}}

### Cartographie des résidus (Bonus : +2 pts)

- **Consigne** : réalisez une carte des résidus standardisés et commentez-là. 

```{r, fig.height=6, fig.width=6}
library(mapsf)

# Standardisation des résidus
tab$Y_res_std<-tab$Y_res/sd(tab$Y_res)

# Jointure avec la carte
map<-map[,c("code","geometry")]
map_reg <- merge(map, tab, by="code")

# Choix de la palette et des classes
library(RColorBrewer)
mypal<-brewer.pal(n = 6, name = "RdYlBu")
mybreaks = c(-10, -2,-1,0,1,2,10)

mf_map(map_reg, type="choro", var="Y_res_std",
       pal = mypal, breaks=mybreaks,
       leg_title = "Résidus standardisés",leg_val_rnd = 1)
mf_layout(title="Ecarts à la tendance 2004-2014",frame = T, credits = "Source : INS Tunisie")

```


- Commentaires



